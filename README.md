This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Motivation

The goal of this lab, is chek how works stagic generator in next.js app.

The requirement is check how works this for SEO in a small drowdownMenu. 

To check static site without use node, you can run the following commands.

```bash
npm run build

npm run export

npx serve out
```

And then go to the chrome inspector and check the HTML. 

As you can see, all the links are in the dom tree even if are hidden, furthermore the website use Javascript hirdration and not use SSR services. 

