import styles from './menu.module.scss'
import {useState} from 'react'

export default function Menu() {

    const [isShow1, setShow1] = useState(true)
    const [isShow2, setShow2] = useState(true)
    const [isShow3, setShow3] = useState(true)
    const [isShow4, setShow4] = useState(true)

 
    return (
        <>
            <div className={styles.container} >
                <div onMouseOver={()=>setShow1(false)} onMouseLeave={()=> setShow1(true)}>
                    Categoria 1
                    
                        <ul hidden={isShow1}>
                            <li><a href="">Link 1.1</a></li>
                            <li><a href="">Link 1.2</a></li>
                            <li><a href="">Link 1.3</a></li>
                        </ul>
                    
                </div>
                <div onMouseOver={()=>setShow2(false)} onMouseLeave={()=> setShow2(true)}>
                    Categoria 2
                 
                        <ul hidden={isShow2}>
                            <li><a href="">Link 2.1</a></li>
                            <li><a href="">Link 2.2</a></li>
                            <li><a href="">Link 2.3</a></li>
                        </ul>
                    
                </div>
                <div onMouseOver={()=>setShow3(false)} onMouseLeave={()=> setShow3(true)}>
                    Categoria 3
                    
                        <ul hidden={isShow3}>
                            <li><a href="">Link 3.1</a></li>
                            <li><a href="">Link 3.2</a></li>
                            <li><a href="">Link 3.3</a></li>
                        </ul>
                    
                </div>
                <div onMouseOver={()=>setShow4(false)} onMouseLeave={()=> setShow4(true)}>
                    Categoria 4
                 
                        <ul hidden={isShow4}>
                            <li><a href="">Link 4.1</a></li>
                            <li><a href="">Link 4.2</a></li>
                            <li><a href="">Link 4.3</a></li>
                        </ul>
                    
                </div>
                
            </div>
        </>

    )
}

